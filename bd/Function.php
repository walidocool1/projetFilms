<?php
require("Connection_BD.php");
error_reporting(E_ALL);


function db_connect(){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  try{
      $connexion=new PDO($dsn,USER,PASSWD);
      $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
  catch(PDOException $e){
    printf("Échec de la connexion : %s\n", $e->getMessage());
    exit();
    }
  return $connexion;
}



// inserer les données sur la base 
function inserer_donner(){

try{
  $connexion = db_connect();
  if($connexion){
  
  // Les réalisateur du films
  $connexion->query(file_get_contents('./bd/individus.sql'));


  //les films (Film ==> réalisateur )
  $connexion->query(file_get_contents('./bd/films.sql'));

  // genres des films
  $connexion->query(file_get_contents('./bd/genres.sql'));

  //classification des films( films => genre)
  $connexion->query(file_get_contents('./bd/classification.sql'));


  //Les acteurs du film
  $connexion->query(file_get_contents('./bd/acteurs.sql'));
  
  //Les membres 
  $connexion->query(file_get_contents('./bd/membres.sql'));
  
 }

  else{
    echo "<script>alert(erreur dans les insertions )</script>";

  }


}
catch(PDOException $ex){
    echo $ex->getMessage();
  }

  }


//Fonction pour avoir tous les attributs des films
function get_all_films(){
  $connexion = db_connect();
  $sql="SELECT * from films JOIN individus on individus.code_indiv=films.realisateur ";
  $data=$connexion->query($sql);
  $films=Array();
  if(!$data) 
   echo "Pb d'accès aux tables";
  else{
    while($row=$data->fetch(PDO::FETCH_ASSOC))
      $films[]=$row;
    }
 return $films;
}


//Fonction pour avoir tous les attributs des films en ayant l'id
function get_films_by_id($id){
    $connexion = db_connect();
    $sql="SELECT * FROM `films` WHERE `code_film` = ?";
    if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }
    $stmt->bindparam(1,$id);
    $stmt->execute();
    return $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
}


//Reourner un film
function get_films(){
    $connexion = db_connect();
    $i=0;
    $sql="SELECT * FROM `films` ";
    if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }
    $stmt->execute();
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC))
    {
      $resultat[$i]=$result;
      $i++;
    }
    return $resultat;
    
}

//Avoir les genres avec le num du film
function get_Genre_Films($numfilm){
  $connexion = db_connect();
  $sql="SELECT distinct(`nom_genre`) from genres , classification where  classification.ref_code_genre=genres.code_genre and classification.ref_code_film=".$numfilm;
  $data=$connexion->query($sql);
  $genre=Array();
  if(!$data) 
   echo "Pb d'accès aux tables";
  else{
    while($row=$data->fetch(PDO::FETCH_ASSOC))
      $genre[]=$row;
    }
 return $genre;
}


//Avoir la liste de tous les genres existants
function listGenres(){
    $connexion = db_connect();
    $sql="SELECT * from genres";
    $data=$connexion->query($sql);
    $genre=Array();
    $i=0;
    if(!$data) 
        echo "Pb d'accès aux tables";
    else
    {
        while($row=$data->fetch(PDO::FETCH_ASSOC))
        {
            $genre[$i]=$row;
            $i++;
        }
           
    }
 return $genre;
}


//Pouvoir actualiser un film deja existant
function updateFilm($data){

     $connexion = db_connect();
     $sql="UPDATE `films` SET `titre_original`=?,`titre_francais`=?,`pays`=?,`date`=?,`duree`=?,`couleur`=?,`realisateur`=? WHERE `code_film`=?";
 
     if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }

    $stmt->bindparam(1,$data['titrevo']);
    $stmt->bindparam(2,$data['titrefr']);
    $stmt->bindparam(3,$data['pays']);
    $stmt->bindparam(4,$data['annee']);
    $stmt->bindparam(5,$data['duree']);
    $stmt->bindparam(6,$data['couleur']);
    $stmt->bindparam(7,$data['realisateur']);
    $stmt->bindparam(8,$data['id']);

    return $stmt->execute();
}


//Pouvoir creer un nouveau film
function createFilm($data){

     $connexion = db_connect();
     $sql="INSERT INTO `films` SET `titre_original`=?,`titre_francais`=?,`pays`=?,`date`=?,`duree`=?,`couleur`=?,`realisateur`=?";
    

     if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }

    $stmt->bindparam(1,$data['titrevo']);
    $stmt->bindparam(2,$data['titrefr']);
    $stmt->bindparam(3,$data['pays']);
    $stmt->bindparam(4,$data['annee']);
    $stmt->bindparam(5,$data['duree']);
    $stmt->bindparam(6,$data['couleur']);
    $stmt->bindparam(7,$data['realisateur']);

    $stmt->execute();
    return $connexion->lastInsertId();
}



//Retourne la liste des genre d'un film
function get_list_genre($idFilm){
    $connexion = db_connect();
    $i=0;

    $sql="SELECT DISTINCT `genres`.`nom_genre` 
    FROM `genres` JOIN `classification` 
    on `genres`.`code_genre` = `classification`.`ref_code_genre` 
    WHERE `classification`.`ref_code_film`= ?";

    if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }
    $stmt->bindparam(1,$idFilm);
    $stmt->execute();
    
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) 
    {
       $resultat[$i] = $result["nom_genre"];
       $i++;
    }

    return $resultat;

}

//Supprimer les genres d'un film existant
function deleteGenre($idFilm){
    $connexion = db_connect();
    $sql = "DELETE FROM `classification` WHERE `ref_code_film` = ?";
    if(!($stmt=$connexion->prepare($sql))) 
    {
        echo "Pb d'accès aux tables";
        return false;
    }
    $stmt->bindparam(1,$idFilm);
    $stmt->execute();
    return true;
}


//Creer une nouvelle liste de genre
function createGenre($data,$idFilm)
{
    $connexion = db_connect();
    $sql = "INSERT INTO `classification` (`ref_code_film`, `ref_code_genre`) VALUES (?,?)";
     

    for ($i=0; $i < count($data); $i++) 
    { 
        if(!($stmt=$connexion->prepare($sql))) 
        {
            echo "Pb d'accès aux tables";
            return false;
        }
        $stmt->bindparam(1,$idFilm);
        $stmt->bindparam(2,$data[$i]);
        $stmt->execute();
    }
    return true;
}



?>
