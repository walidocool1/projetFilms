-- phpMyAdmin SQL Dump
-- version 4.4.2
-- http://www.phpmyadmin.net
--
-- Client :  servinfo-db
-- Généré le :  Ven 17 Avril 2015 à 10:58
-- Version du serveur :  5.5.41-MariaDB-1ubuntu0.14.04.1
-- Version de PHP :  5.5.9-1ubuntu4.4




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `dbgerard`
--

-- --------------------------------------------------------

--
-- Structure de la table `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `code_genre` int(11) NOT NULL,
  `nom_genre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `genres`
--

INSERT INTO `genres` (`code_genre`, `nom_genre`) VALUES
(26, 'Vieux                           '),
(4, 'Futuriste                        '),
(5, 'Pas drole                        '),
(6, 'Humaniste                        '),
(10, 'Jeu                             '),
(15, 'Poésie                          '),
(11, 'La France                       '),
(7, 'Rigolo                           '),
(28, 'Documentaire Animalier          '),
(17, 'Bizarre                         '),
(19, 'Verité                          '),
(20, 'Paris                           '),
(14, 'Culte                           '),
(13, 'Enfants                         '),
(9, 'Musical                          '),
(23, 'Los Angeles & Hollywood         '),
(3, 'Horreur                          '),
(21, 'Critique sociale                '),
(22, 'Epique                          '),
(27, 'Moyen-Age		                  '),
(12, 'New-York                        '),
(1, 'Malheureux                       '),
(30, 'Bollywooderie                   '),
(16, 'Fantastique                     '),
(25, 'Documentaire                    '),
(8, 'Historique                       '),
(2, 'Humoristique                     '),
(29, 'Aventure                        '),
(24, 'Action                          ');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`code_genre`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `genres`
--
ALTER TABLE `genres`
  MODIFY `code_genre` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
