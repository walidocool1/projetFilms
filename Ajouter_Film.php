<?php
	session_start();
	require("./bd/Function.php");
	include("header.php");
	$tabGenre =listGenres();
	$modif = "Ajouter le film";
	if(isSet($_POST['id']))
	{
		$modif = "Modifier le film";
		if($dataFilm = get_films_by_id($_POST['id']))
		{
			$listeGenre = get_list_genre($dataFilm['code_film']);
		}
	}

?>

<body>
	<form action="UpdateFilm.php" method="POST" class="form-horizontal" id="formulaire">
		
		<input name="id" type="hidden" value ="<?php if (isset ( $dataFilm['code_film'])) echo $dataFilm['code_film'] ?>" >

		<div class="form-group">
			<label for="titrevo" class="col-sm-2 control-label">Titre original du film: </label>
			<div class="col-sm-5">
      			<input type="text" class="form-control" name="titrevo"
      			required value="<?php if(isSet($dataFilm['titre_original'])) echo $dataFilm['titre_original'] ?>" /></br>
    		</div>
  		</div>

  		<div class="form-group">
			<label for="titrefr" class="col-sm-2 control-label">Titre français du film: </label>
			<div class="col-sm-5">
      			<input type="text" class="form-control" name="titrefr"
      			required value="<?php if(isSet($dataFilm['titre_francais'])) echo $dataFilm['titre_francais'] ?>" /></br>
    		</div>
  		</div>

  		<div class="form-group">
			<label for="pays" class="col-sm-2 control-label">Pays:</label>
			<div class="col-sm-5">
      			<input type="text" class="form-control" name="pays"
      			required value="<?php if(isSet($dataFilm['pays'])) echo $dataFilm['pays'] ?>" /></br>
    		</div>
  		</div>

  		<div class="form-group">
			<label for="annee" class="col-sm-2 control-label">Année de sortie:</label>
			<div class="col-sm-5">
      			<input type="text" name="annee" size="4" maxlength="4" pattern="[0-9]{4}?" class="form-control"
      			title="Entrez un nombre entier à 4 chiffres"
      			required  value="<?php if(isSet($dataFilm['date'])) echo $dataFilm['date'] ?>" /></br>
    		</div>
  		</div>

  		<div class="form-group">
			<label for="duree" class="col-sm-2 control-label">Durée du film en minute:</label>
			<div class="col-sm-5">
      			<input type="text" name="duree" size="4" maxlength="4" pattern="[0-9]{1,4}?" class="form-control"
      			title="Entrez un nombre entier de 1 à 4 chiffres"
      			required value="<?php if(isSet($dataFilm['duree'])) echo $dataFilm['duree'] ?>" /></br>
    		</div>
  		</div>

  		<div class="form-group">
			<label for="couleur" class="col-sm-2 control-label">Style de film:</label>
			<div class="col-sm-5">
      			<input type="text" class="form-control" name="couleur"
      			required value="<?php if(isSet($dataFilm['couleur'])) echo $dataFilm['couleur'] ?>" /></br>
    		</div>
  		</div>


  		<div class="form-group">
			<label for="realisateur" class="col-sm-2 control-label">Réalisateur:</label>
			<div class="col-sm-5">
      			<input type="text" class="form-control" name="realisateur"
      			required value="<?php if(isSet($dataFilm['realisateur'])) echo $dataFilm['realisateur'] ?>" /></br>
    		</div>
  		</div>


  		<div class="form-group" >
	  		<label for="genre" class="col-sm-2 control-label">Genre:</label>
			<div class="col-sm-5">
				<select name="cb[]"  multiple size=8 class="form-control">
					<?php 
						for ($i=0; $i < count($tabGenre); $i++) 
						{ 
				 			echo "<option class=\"form-control\" value=".$tabGenre[$i]["code_genre"].">".$tabGenre[$i]["nom_genre"]."</option>";
						}
					?>   
				</select> 
			</div>
  		</div>
		
		<br/>
		
		<input style="display:block;margin:auto;" type="submit" class="btn btn-primary" value="<?php echo $modif ?>"/>
	</form>

</body>
</html>