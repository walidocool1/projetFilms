<?php 
session_start();
        
?>
	
	<?php
	// mettre la barre d'identification
		include("header.php");
	?>
	<head>
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
		<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<script src="./css/jquery/js/jquery.dataTables.columnFilter.js"></script>
	</head>
	<body>
		
		<?php
			try{
				// Connexion à la base
				require("./bd/Function.php");
				$file_db=db_connect();
				
				//include("rechercheCritere.php");
				$result =get_films();
				echo "<table  class=\"table table-striped\" id=\"dataTable\">";
				echo "<thead>";
				echo "<tr>";
				echo "<th class=\"hide\"></th>";
				echo "<th> Titre original</th>";
				echo "<th> Pays </th>";
				echo "<th> Année de sortie </th>";
				echo "<th> Durée </th>";
				echo "<th> Genres </th>";
				echo "<th></th>";
				echo "</thead>";
				echo "</tr>";
				
				foreach ($result as $listfilm){
					echo "<tr>";
					?>
					<form method='Post' action='Ajouter_Film.php'>
					<td class='hide' >
						<input name="id" type="hidden" value ="<?php echo $listfilm['code_film']; ?>" >
					</td>

					<td name="titre_original"  ><?php echo $listfilm['titre_original']; ?></td>

					<td name="pays"  ><?php echo $listfilm['pays']; ?></td>

					<td name="date"  ><?php echo $listfilm['date']; ?></td>

					<td name="duree"  ><?php echo $listfilm['duree']; ?></td>

					<?php
					
					$res=get_Genre_Films($listfilm['code_film']);
					echo "<td>";
					foreach ($res as $listgenre){
						echo "<ul class=\"list-unstyled\" >";
						foreach ($listgenre as $key => $value) {
							echo " <li> $value </li>";

						}

						echo "</ul";
					}
					?>
					<!-- quand l'utilisateur supprime un film on peut avoir le code du film -->
					<td>
					
					</br>

					<input type=submit value="modifier" 
					<?php
					
					//verifier si l'utilisateur est connecter ou pas 
					if( $_SESSION['connecte']!=true){
						echo "style='display:none' action ";
						}
						?>
					/> 
					
					</br>
					</td>

					</form>
					<?php
					echo "</td>";
					echo "</tr>";	
				}

				echo "<tfoot>";
				echo "<tr>";
				echo "<th class=\"hide\"></th>";
				echo "<th> Titre original</th>";
				echo "<th> Pays </th>";
				echo "<th> Année de sortie </th>";
				echo "<th> Durée </th>";
				echo "<th> Genres </th>";
				echo "<th></th>";
				echo "</tr>";
				echo "</tfoot>";
				echo "</table>";
				
				$file_db=null;
			}
			catch(PDOException $ex){
				echo $ex->getMessage();
			}
		?>
	</body>

<script type="text/javascript">

$(document).ready(function(){
    $('#dataTable').dataTable().columnFilter();

});

</script>
		
		
